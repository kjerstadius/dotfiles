require('telescope').setup {
  extensions = {
    cder = {
      previewer_command =
        'exa '..
        '--all '..
        '--color=always '..
        '--git '..
        '--icons '..
        '--ignore-glob=.git '..
        '--long '..
        '--no-filesize '..
        '--no-permissions '..
        '--no-user '..
        '--tree '..
        '--level=3'
    }
  }
}

local keymap = vim.keymap
keymap.set('n', '<leader>cd', function() require('telescope').extensions.cder.cder() end)
keymap.set('n', '<leader>fb', function() require('telescope.builtin').buffers() end)
keymap.set('n', '<leader>ff', function() require('telescope.builtin').find_files() end)
keymap.set('n', '<leader>fs', function() require('telescope.builtin').grep_string() end)
keymap.set('n', '<leader>fg', function() require('telescope.builtin').live_grep() end)
keymap.set('n', '<leader>fh', function() require('telescope.builtin').help_tags() end)
keymap.set('n', '<leader>fr', function() require('telescope').extensions.frecency.frecency() end)
keymap.set({'n', 'i'}, '<C-Space>', function() require('telescope').extensions.luasnip.luasnip() end)
