require("catppuccin").setup {
    flavour = "macchiato",
    background = {
        dark = "macchiato",
        light = "latte"
    },
    integrations = {
        cmp = true,
        gitsigns = true,
        indent_blankline = {
            enabled = true
        },
        leap = true,
        markdown = true,
        native_lsp = {
            enabled = true
        },
        neogit = true,
        telescope = true,
        treesitter = true,
        ts_rainbow = true
    }
}
vim.api.nvim_command "colorscheme catppuccin"
