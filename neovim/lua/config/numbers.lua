require('numbers').setup({
  excluded_filetypes = {
    'alpha'
  },
  relative_events = {
    'BufEnter',
    'FocusGained',
    'InsertLeave',
    'WinEnter'
  }
})
