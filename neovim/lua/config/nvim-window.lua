require('nvim-window').setup {
  chars = {
    'n', 'e', 't', 'a', 's', 'i', 'r', 'h'
  }
}

vim.keymap.set('n', '<leader>w', function() require('nvim-window').pick() end, {silent = true})
