-- originally authored by @AdamWhittingham

local path_ok, plenary_path = pcall(require, "plenary.path")
if not path_ok then
  return
end

local dashboard = require("alpha.themes.dashboard")
local max_item_width = 50
local path_margin = 10
local if_nil = vim.F.if_nil

local nvim_web_devicons = {
    enabled = true,
    highlight = true,
}

local function get_extension(fn)
    local match = fn:match("^.+(%..+)$")
    local ext = ""
    if match ~= nil then
        ext = match:sub(2)
    end
    return ext
end

local function icon(fn)
    local nwd = require("nvim-web-devicons")
    local ext = get_extension(fn)
    return nwd.get_icon(fn, ext, { default = true })
end

local function file_button(fn, sc, short_fn)
    short_fn = short_fn or fn
    local ico_txt
    local fb_hl = {}

    if nvim_web_devicons.enabled then
        local ico, hl = icon(fn)
        local hl_option_type = type(nvim_web_devicons.highlight)
        if hl_option_type == "boolean" then
            if hl and nvim_web_devicons.highlight then
                table.insert(fb_hl, { hl, 0, 3 })
            end
        end
        if hl_option_type == "string" then
            table.insert(fb_hl, { nvim_web_devicons.highlight, 0, 3 })
        end
        ico_txt = ico .. "  "
    else
        ico_txt = ""
    end
    local file_button_el = dashboard.button(sc, ico_txt .. short_fn, "<cmd>e " .. fn .. " <CR>")
    file_button_el.opts.width = max_item_width
    local fn_start = short_fn:match(".*[/\\]")
    if fn_start ~= nil then
        table.insert(fb_hl, { "Comment", #ico_txt - 2, #fn_start + #ico_txt })
        table.insert(fb_hl, { "Macro", #fn_start + #ico_txt, #fn + #ico_txt })
    else
        table.insert(fb_hl, { "Macro", #ico_txt, #fn })
    end
    file_button_el.opts.hl = fb_hl
    file_button_el.opts.hl_shortcut = "Typedef"
    return file_button_el
end

---@param path any path to be shortened
---@param target_width integer target maximum width (not guaranteed)
---@param cwd string? optional current directory path
local function shorten_path(path, target_width, cwd)
    local short_path
    if cwd then
        short_path = vim.fn.fnamemodify(path, ":.")
    else
        short_path = vim.fn.fnamemodify(path, ":~")
    end

    if(#short_path > target_width) then
      short_path = plenary_path.new(short_path):shorten(1, {-2, -1})
      if(#short_path > target_width) then
        short_path = plenary_path.new(short_path):shorten(1, {-1})
      end
    end
    return short_path
end

local default_mru_ignore = { "gitcommit" }

local mru_opts = {
    ignore = function(path, ext)
        return (string.find(path, "COMMIT_EDITMSG")) or (vim.tbl_contains(default_mru_ignore, ext))
    end,
}

--- @param start number
--- @param cwd string? optional
--- @param items_number number? optional number of items to generate, default = 10
local function mru(start, cwd, items_number, opts)
    opts = opts or mru_opts
    items_number = if_nil(items_number, 10)

    local oldfiles = {}
    for _, v in pairs(vim.v.oldfiles) do
        if #oldfiles == items_number then
            break
        end
        local cwd_cond
        if not cwd then
            cwd_cond = true
        else
            cwd_cond = vim.startswith(v, cwd)
        end
        local ignore = (opts.ignore and opts.ignore(v, get_extension(v))) or false
        if (vim.fn.filereadable(v) == 1) and cwd_cond and not ignore then
            oldfiles[#oldfiles + 1] = v
        end
    end

    local tbl = {}
    for i, fn in ipairs(oldfiles) do
        local short_fn = shorten_path(fn, max_item_width - path_margin, cwd)

        local shortcut = tostring(i+start-1)

        local file_button_el = file_button(fn, shortcut, short_fn)
        tbl[i] = file_button_el
    end
    return {
        type = "group",
        val = tbl,
        opts = {},
    }
end

local default_header = {
    type = "text",
    val = {
        [[                               __]],
        [[  ___     ___    ___   __  __ /\_\    ___ ___]],
        [[ / _ `\  / __`\ / __`\/\ \/\ \\/\ \  / __` __`\]],
        [[/\ \/\ \/\  __//\ \_\ \ \ \_/ |\ \ \/\ \/\ \/\ \]],
        [[\ \_\ \_\ \____\ \____/\ \___/  \ \_\ \_\ \_\ \_\]],
        [[ \/_/\/_/\/____/\/___/  \/__/    \/_/\/_/\/_/\/_/]],
    },
    opts = {
        position = "center",
        hl = "Character",
    },
}


local section_mru = {
  type = "group",
  val = {
    {
      type = "text",
      val = "Most recent",
      opts = {
        hl = "Type",
        shrink_margin = false,
        position = "center",
      },
    },
    { type = "padding", val = 1 },
    {
      type = "group",
      val = function()
        return { mru(0) }
      end,
      opts = { shrink_margin = false },
    },
  }
}

local function mru_cwd_title()
    return "Recent files in " .. shorten_path(vim.fn.getcwd(), max_item_width)
end

local section_mru_cwd = {
  type = "group",
  val = {
    {
      type = "text",
      val = mru_cwd_title,
      opts = {
        hl = "Type",
        shrink_margin = false,
        position = "center",
      },
    },
    { type = "padding", val = 1 },
    {
      type = "group",
      val = function()
        return { mru(10, vim.fn.getcwd(), 5) }
      end,
      opts = { shrink_margin = false },
    },
  }
}

---@param shortcut string shortcut
---@param description string text description
---@param command string? optional command to execute for shortcut
local function ql_button(shortcut, description, command)
    local button = dashboard.button(shortcut, description, command)
    button.opts.width = max_item_width
    button.opts.hl = {
        { "Type", 0, 3 },
        { "Text", 3, #description }
    }
    button.opts.hl_shortcut = "Float"
    return button
end

local buttons = {
  type = "group",
  val = {
    { type = "text", val = "Quick links", opts = { hl = "Type", position = "center" } },
    { type = "padding", val = 1 },
    ql_button("e", "  New file", "<cmd>ene<CR>"),
    ql_button("BSL f f", "  Find file"),
    ql_button("BSL f g", "  Live grep"),
    ql_button("BSL f r", "⏲  Frecency"),
    ql_button("BSL o m", "  Org mode (Neorg)"),
    ql_button("c", "  Configuration", "<Cmd>cd ~/.config/nvim/ <CR>"),
    ql_button("u", "  Update plugins" , "<Cmd>PackerSync<CR>"),
    ql_button("q", "  Quit" , "<Cmd>qa<CR>"),
  },
  position = "center",
}

local config = {
    layout = {
        { type = "padding", val = 2 },
        default_header,
        { type = "padding", val = 2 },
        section_mru,
        { type = "padding", val = 2 },
        section_mru_cwd,
        { type = "padding", val = 2 },
        buttons,
    },
    opts = {
        margin = 5,
        setup = function()
            vim.api.nvim_create_autocmd("DirChanged", {
                pattern = "*",
                callback = function() require("alpha").redraw() end
            })
        end
    },
}

return {
    config = config,
    nvim_web_devicons = nvim_web_devicons,
}
