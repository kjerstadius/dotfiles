local luasnip = require('luasnip')
local types = require('luasnip.util.types')
local keymap = vim.keymap

luasnip.config.setup {
  ext_opts = {
    [types.choiceNode] = {
      active = {
        virt_text = {{'●', 'DraculaOrange'}}
      }
    },
    [types.insertNode] = {
      active = {
        virt_text = {{'●', 'DraculaBlue'}}
      }
    }
  }
}

require('luasnip.loaders.from_vscode').lazy_load()
luasnip.add_snippets(nil, require('luasnip_snippets').load_snippets())


keymap.set({'i', 's'}, '<C-n>', '<Plug>luasnip-next-choice')
keymap.set({'i', 's'}, '<C-p>', '<Plug>luasnip-prev-choice')
