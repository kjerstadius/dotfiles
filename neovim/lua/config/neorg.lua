local keymap = vim.keymap

require('neorg').setup {
  load = {
    ["core.defaults"] = {},
    ["core.norg.dirman"] = {
      config = {
        workspaces = {
          notes = '~/documents/notes',
          gtd = '~/documents/gtd'
        },
        autochdir = true
      }
    },
    ['core.gtd.base'] = {
      config = {
        workspace = 'gtd'
      }
    },
    ['core.norg.concealer'] = {
      config = {
        preset = 'varied'
      }
    }
  }
}


keymap.set('n', '<leader>om', '<Cmd>NeorgStart<CR>')
