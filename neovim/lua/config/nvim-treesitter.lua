require('nvim-treesitter.configs').setup {
  ensure_installed = {
    'bash',
    'c',
    'cmake',
    'comment',
    'cpp',
    -- devicetree, annoyingly requires node js for some reason
    'dockerfile',
    'fish',
    'json',
    'latex',
    'lua',
    'make',
    'markdown',
    'ninja',
    'norg',
    'python',
    'rasi',
    'regex',
    'rst',
    'rust',
    'toml',
    'vim',
    'yaml',
    'zig'
  },
  highlight = { enable = true },
  rainbow = {
    enable = true,
    extended_mode = true,
    max_file_lines = nil
  }
}
