local fn = vim.fn

-- Bootstrap Packer
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  -- Workaround for runtimepath changes in 0.7.0
  -- See https://github.com/wbthomason/packer.nvim/issues/750
  vim.o.runtimepath = fn.stdpath('data')..'/site/pack/*/start/*'..vim.o.runtimepath
end

return require('packer').startup(function(use)
  -- Let Packer manage itself
  use 'wbthomason/packer.nvim'

  -- Look and feel
  use {
    "catppuccin/nvim", as = "catppuccin",
    config = function() require("config.catppuccin") end
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
    config = function() require('config.lualine') end
  }

  -- Windows and navigation
  use {
    {
      'ggandor/leap.nvim',
      requires = 'tpope/vim-repeat',
      config = function() require('leap').add_default_mappings() end
    },
    {
      'https://gitlab.com/yorickpeterse/nvim-window',
      config = function() require('config.nvim-window') end
    }
  }

  -- Fancy highlighting
  use {
    'nvim-treesitter/nvim-treesitter',
    requires = { 'p00f/nvim-ts-rainbow' },
    run = ':TSUpdate',
    config = function() require('config.nvim-treesitter') end
  }

  -- LSP
  use {
    {
      'neovim/nvim-lspconfig',
      config = function() require('config.lsp') end
    },
    { 'p00f/clangd_extensions.nvim' },
    {
        'andrewferrier/textobj-diagnostic.nvim',
        config = function() require('textobj-diagnostic').setup() end
    },
    {
      "smjonas/inc-rename.nvim",
      config = function()
        require("inc_rename").setup()
        vim.keymap.set("n", "<leader>rn", ":IncRename ")
      end
    }
  }

  -- Completion and snippets
  use {
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'saadparwaiz1/cmp_luasnip',
      { 'L3MON4D3/LuaSnip', config = function() require('config.luasnip') end },
      'rafamadriz/friendly-snippets',
      { 'kjerstadius/LuaSnip-snippets.nvim', branch = 'fix/user-args-table' }
    },
    config = function() require('config.cmp') end
  }

  -- Telescope
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        config = function() require('telescope').load_extension('fzf') end,
        run = [[
          cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release -G Ninja \
          && cmake --build build --config Release \
          && cmake --install build --prefix build
        ]]
      },
      {
        'nvim-telescope/telescope-frecency.nvim',
        requires = 'tami5/sqlite.lua',
        config = function() require('telescope').load_extension('frecency') end
      },
      {
        'zane-/cder.nvim',
        config = function() require('telescope').load_extension('cder') end
      },
      {
        'benfowler/telescope-luasnip.nvim',
        config = function() require('telescope').load_extension('luasnip') end
      }
    },
    config = function() require('config.telescope') end
  }

  -- Startup/dashboard
  use {
    'goolord/alpha-nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
    config = function ()
      require('alpha').setup(require('config.alpha.themes.theta').config)
    end
  }

  -- Org mode
  use {
    'nvim-neorg/neorg',
    requires = 'nvim-lua/plenary.nvim',
    config = function() require('config.neorg') end
  }

  -- git
  use {
    { 'tpope/vim-fugitive' },
    {
      'lewis6991/gitsigns.nvim',
      config = function() require('gitsigns').setup { current_line_blame = true } end
    },
    {
      'TimUntersberger/neogit',
      requires = 'nvim-lua/plenary.nvim',
      config = function() require('neogit').setup { disable_signs = true } end
    }
  }

  -- Kitty integration
  use {
    'jghauser/kitty-runner.nvim',
    config = function() require('kitty-runner').setup() end
  }

  -- Misc
  use 'tpope/vim-commentary'
  use {
    'lukas-reineke/indent-blankline.nvim',
    config = function() require('indent_blankline').setup() end
  }
  use {
    'nkakouros-original/numbers.nvim',
    config = function() require('config.numbers') end
  }

  -- Faster startup
  use 'lewis6991/impatient.nvim'

  -- Plugins to look into:
  -- Better folding: https://github.com/kevinhwang91/nvim-ufo
  -- Hydra: https://github.com/anuvyklack/hydra.nvim
  -- Text case conversion: https://github.com/johmsalas/text-case.nvim
  -- null-ls: https://github.com/jeso-elias-alvarez/null-ls.nvim
  -- trouble: https://github.com/folke/trouble.nvim
  -- nvim-surround: https://github.com/kylechui/nvim-surround
  -- Better comments? https://github.com/numToStr/Comment.nvim
  -- multiline diagnostics: https://git.sr.ht/~whynothugo/lsp_lines.nvim

  if packer_bootstrap then
    require('packer').sync()
  end
end)
