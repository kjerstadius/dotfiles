-- Note: Requires Neovim 0.7.0 or higher

-- Load impatient first to ensure it can do its thing
require('impatient')

-- Include modules
require('plugins')


-- Aliases
local cmd = vim.cmd
local create_augroup = vim.api.nvim_create_augroup
local create_autocmd = vim.api.nvim_create_autocmd
local keymap = vim.keymap
local opt = vim.opt
local opt_local = vim.opt_local


-- General settings
opt.expandtab = true
opt.shiftwidth = 4
opt.smartindent = true
opt.tabstop = 4

opt.splitbelow = true
opt.splitright = true

opt.tags:append({'~/code'})


-- Keybinds
-- Trim trailing whitspace
-- Taken from https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-trailing-whitespace-from-all-lines-in-a-file
keymap.set('n', '<F3>', [[:let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>]])

-- Spelling
keymap.set('n', '<F4>', function() opt_local.spell = false end)
keymap.set('n', '<F5>', function() opt_local.spell = true; opt_local.spelllang = 'en_us' end)
keymap.set('n', '<F6>', function() opt_local.spell = true; opt_local.spelllang = 'sv' end)


local tr_whitespace_grp = create_augroup('tr_whitespace', {clear=true})
create_autocmd(
  {'BufWinEnter', 'InsertLeave'},
  {
    buffer = 0,
    group = 'tr_whitespace',
    callback = function() cmd([[match ErrorMsg /\s\+$/]]) end
  }
)
create_autocmd(
  {'InsertEnter'},
  {
    buffer = 0,
    group = 'tr_whitespace',
    callback = function() cmd([[match ErrorMsg /\s\+\%#\@<!$/]]) end
  }
)
create_autocmd(
  {'BufWinLeave'},
  {
    buffer = 0,
    group = 'tr_whitespace',
    callback = function() cmd([[call clearmatches()]]) end
  }
)
