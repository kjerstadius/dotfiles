# Startup
exec --no-startup-id mako

default_border pixel 2
smart_borders on
gaps inner 4
gaps outer 0
smart_gaps on

input type:keyboard {
    xkb_layout eu
}

workspace "" output $primary
workspace "" output $primary
workspace "" output $primary
workspace "" output $primary
workspace "" output $secondary
workspace "" output $secondary
workspace "" output $secondary

for_window [title="GOG Galaxy *"] floating enable
for_window [class="kcalc"] border pixel 2
for_window [class="Lutris"] floating enable
for_window [class="makemkv"] floating enable
for_window [class="MPlayer"] floating enable
for_window [class="rbdoom3bfg"] fullscreen enable
for_window [class="RPCS3"] floating enable
for_window [class="Steam"] floating enable
for_window [class="steam_app_*"] floating enable
for_window [class="steam_app_*"] inhibit_idle focus
for_window [class="vlc"] floating enable

assign [class="discord"] ""
assign [class="Steam"] ""
assign [class="steam_app_*"] ""

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:fantasque sans mono 8

# use these keys for focus, movement, and resize directions when reaching for
# the arrows is not convenient
set $up aring
set $down minus
set $left odiaeresis
set $right adiaeresis

set $mod Mod4
set $Alt Mod1

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec kitty

# start browser
bindsym $mod+b exec qutebrowser

# kill focused window
bindsym $mod+Shift+q kill

set $menu_cmd rofi \
    -modi 'drun,run,ssh' \
    -show drun \
    -run-lists-command 'fish -c functions' \
    -run-shell-command '{terminal} -e fish -ic "{cmd}"' \
    -parse-hosts \
    -ssh-command '{terminal} -e fish -c "{ssh-client} {host}"' \
    -terminal kitty
bindsym $mod+d exec --no-startup-id $menu_cmd
bindsym $Alt+space exec --no-startup-id $menu_cmd

# Alt-tab-ish
bindsym $mod+Tab exec --no-startup-id rofi -show window

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# move the currently focused window to the scratchpad
bindsym $mod+Shift+period move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+period scratchpad show

# Log out menu
bindsym $mod+Escape exec --no-startup-id ~/code/scripts/rofi/powermenu.sh

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $wsgame ""
set $ws1 ""
set $ws2 ""
set $ws3 ""
set $ws4 ""
set $ws5 ""
set $ws6 ""
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+grave workspace $wsgame
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+grave move container to workspace $wsgame
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# Reload the configuration file
bindsym $mod+Shift+c reload

# Exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

# media controls
bindsym $mod+p exec --no-startup-id mpc -p 6660 toggle
bindsym $mod+i exec --no-startup-id mpc -p 6660 prev
bindsym $mod+o exec --no-startup-id mpc -p 6660 next
bindsym $mod+Shift+i exec --no-startup-id mpc -p 6660 seek -10
bindsym $mod+Shift+o exec --no-startup-id mpc -p 6660 seek +10

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym $left       resize shrink width 10 px or 5 ppt
        bindsym $down       resize grow height 10 px or 5 ppt
        bindsym $up         resize shrink height 10 px or 5 ppt
        bindsym $right      resize grow width 10 px or 5 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width 10 px or 5 ppt
        bindsym Down        resize grow height 10 px or 5 ppt
        bindsym Up          resize shrink height 10 px or 5 ppt
        bindsym Right       resize grow width 10 px or 5 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

# Catppuccin Macchiato theme colors
# Taken from https://github.com/catppuccin/catppuccin
set $background #24273a
set $bg-light   #494d64
set $foreground #cad3f5
set $blue       #b7bdf8
set $cyan       #8bd5ca
set $green      #a6da95
set $orange     #f5a97f
set $pink       #f5bde6
set $purple     #c6a0f6
set $red        #ed8796
set $yellow     #eed49f
set $fg-dark    #a5adcb

bar {
    swaybar_command waybar
}

# property              border      bground     text        indicator   child_border
client.focused          $blue       $blue       $foreground $blue       $blue
client.focused_inactive $bg-light   $bg-light   $foreground $bg-light   $bg-light
client.unfocused        $background $background $fg-dark    $background $background
client.urgent           $bg-light   $red        $foreground $red        $red
client.placeholder      $background $background $foreground $background $background
client.background       $foreground
