function sysmon
    watch -n 5 "sensors |rg SMBUSMASTER\|fan1\|fan2 \
        |sed -e 's/SMBUSMASTER 0:/CPU:          /g' \
    && echo \
    && cat /proc/cpuinfo |rg MHz \
    && echo && nvidia-smi"
end
