function ssh-verify-host --description 'Get the ssh key fingerprint(s) of a given host for verification and optionally add to known hosts.'
    if test "$argv" = ""
        echo 'Missing argument "hostname". Exiting.'
        return 1
    end
 
    ssh-keyscan -H $argv > /tmp/keys-temp 2> /dev/null
    ssh-keygen -l -f /tmp/keys-temp
    echo

    set loop true
    while $loop
        read -l -P "Do you want to add the key(s) above to your known hosts? Only do this if you're certain that the fingerprint is valid. [y/N] " choice
        
        switch $choice
            case y Y
                cat /tmp/keys-temp >> ~/.ssh/known_hosts
                echo 'Key fingerprint(s) added to ~/.ssh/known_hosts.'
            case n N ''
                echo 'Key fingerprint(s) not added to known hosts.'
        end
        set loop false
    end
    
    rm /tmp/keys-temp
end
                
