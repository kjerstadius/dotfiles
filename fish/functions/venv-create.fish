function venv-create
    if test -e ~/.venvs/$argv
        echo "Virtual environment directory already exists. Doing nothing."
    else
        python -m venv ~/.venvs/$argv
    end
end

