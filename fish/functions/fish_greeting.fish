function fish_greeting
    if command --search fortune >/dev/null; and command --search cowsay >/dev/null; and command --search lolcat >/dev/null end
        fortune -s |cowsay |lolcat
    else
        true
    end
end
