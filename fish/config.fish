alias cdi='ranger-cd'
alias e='nvim'
alias f='fuck'
alias fm='ranger'
alias fmcd='ranger-cd'
alias l='ll'
alias la='ll --all'
alias ll='exa \
    --binary \
    --classify \
    --color-scale \
    --git \
    --group \
    --group-directories-first \
    --long'
alias lla='ll --all'
alias lsz='ll --reverse --sort=size'
alias tree='ll --tree'

thefuck --alias | source

if test -z "$SSH_ENV"
    set -xg SSH_ENV $HOME/.ssh/environment
end

if not __ssh_agent_is_started
    __ssh_agent_start
end

eval (starship init fish)
