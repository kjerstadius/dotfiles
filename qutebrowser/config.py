import catppuccin

# Load existing settings made via :set
config.load_autoconfig()

catppuccin.setup(c, 'macchiato')
# Tweak some of the settings
c.colors.tabs.even.bg = "#494d64"
c.colors.tabs.even.fg = "#a5abcb"
c.colors.tabs.odd.fg = "#a5abcb"
c.colors.tabs.pinned.even.bg = "#a6da95"
c.colors.tabs.pinned.odd.bg = "#a6da95"
c.colors.tabs.pinned.even.fg = "#363a4f"
c.colors.tabs.pinned.odd.fg = "#363a4f"
c.colors.tabs.pinned.selected.even.bg = "#1e2030"
c.colors.tabs.pinned.selected.odd.bg = "#1e2030"
c.colors.tabs.pinned.selected.even.fg = "#cad3f5"
c.colors.tabs.pinned.selected.odd.fg = "#cad3f5"
c.colors.messages.error.bg = "#363a4f"
c.colors.messages.warning.bg = "#363a4f"
c.colors.messages.info.bg = "#363a4f"

padding = {
    'top': 6,
    'right': 6,
    'bottom': 6,
    'left': 6
}

c.statusbar.padding = padding
c.tabs.padding = padding
c.tabs.favicons.scale = 1
c.tabs.indicator.width = 1

c.auto_save.session = True
c.editor.command = ['kitty', '--single-instance', 'nvim', '{}']
c.hints.chars = 'rsntgaeih'
c.hints.selectors["frame"] = ['div', 'header', 'nav', 'section']

config.bind('<Ctrl-e>', 'hint frame', mode='normal')
config.bind('e', 'hint all current', mode='normal')
config.bind('E', 'hint all tab', mode='normal')
config.bind(',m', 'spawn ~/code/scripts/umpv {url}')
config.bind(',M', 'hint links spawn ~/code/scripts/umpv {hint-url}')
config.bind(';M', 'hint --rapid links spawn ~/code/scripts/umpv {hint-url}')
