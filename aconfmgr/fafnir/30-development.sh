# From base-devel group
AddPackage autoconf # A GNU tool for automatically configuring source code
AddPackage automake # A GNU tool for automatically creating Makefiles
AddPackage bison # The GNU general-purpose parser generator
AddPackage fakeroot # Tool for simulating superuser privileges
AddPackage patch # A utility to apply patch files to original sources
AddPackage pkgconf # Package compiler and linker metadata toolkit
AddPackage texinfo # GNU documentation system for on-line information and printed output

# Build systems
AddPackage cmake # A cross-platform open-source make system
AddPackage make # GNU make utility to maintain groups of programs
AddPackage meson # High productivity build system
AddPackage ninja # Small build system with a focus on speed

# Version control systems
AddPackage git # the fast distributed version control system
AddPackage git-lfs # Git extension for versioning large files
AddPackage subversion # A Modern Concurrent Version Control System

# Host compilers
AddPackage clang # C language family frontend for LLVM
AddPackage gcc # The GNU Compiler Collection - C and C++ frontends

# Cross-compile toolchains and utils
AddPackage arm-none-eabi-binutils # A set of programs to assemble and manipulate binary and object files for the ARM EABI (bare-metal) target
AddPackage arm-none-eabi-gcc # The GNU Compiler Collection - cross compiler for ARM EABI (bare-metal) target
AddPackage arm-none-eabi-gdb # The GNU Debugger for the ARM EABI (bare-metal) target
AddPackage arm-none-eabi-newlib # A C standard library implementation intended for use on embedded systems (ARM bare metal)
AddPackage avr-gcc # The GNU AVR Compiler Collection
AddPackage --foreign avr-libc-avrxmega3-svn # The C runtime library for the AVR family of microcontrollers (incl. avrxmega3 devices)

# Flashing tools
AddPackage dfu-programmer # Programmer for Atmel chips with a USB bootloader
AddPackage dfu-util # Tool intended to download and upload firmware using DFU protocol to devices connected over USB
CopyFile /etc/udev/rules.d/50-atmel-dfu.rules

# Languages
AddPackage lua # Powerful lightweight programming language designed for extending applications
AddPackage perl # A highly capable, feature-rich programming language
AddPackage python # Next generation of the python high-level scripting language
AddPackage python2 # A high-level scripting language
AddPackage rust # Systems programming language focused on safety, speed and concurrency

# Language specific
AddPackage leiningen # Automate Clojure projects

# Containers
AddPackage buildah # A tool which facilitates building OCI images
AddPackage podman # Tool and library for running OCI-based containers in pods
AddPackage runc # CLI tool for managing OCI compliant containers
AddPackage skopeo # A command line utility for various operations on container images and image repositories.

# Web
AddPackage geckodriver # Proxy for using W3C WebDriver-compatible clients to interact with Gecko-based browsers.
AddPackage python-selenium # Python binding for Selenium Remote Control

# Pentest
AddPackage cracklib # Password Checking Library
AddPackage john # John the Ripper password cracker

# Misc
AddPackage archiso # Tools for creating Arch Linux live and install iso images
AddPackage ctags # Generates an index file of language objects found in source files
AddPackage minicom # A serial communication program
AddPackage python-sphinx # Python documentation generator
AddPackage sqlitebrowser # SQLite Database browser is a light GUI editor for SQLite databases, built on top of Qt
AddPackage strace # A diagnostic, debugging and instructional userspace tracer
