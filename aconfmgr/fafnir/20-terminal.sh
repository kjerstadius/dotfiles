# Shell
AddPackage fish # Smart and user friendly shell intended mostly for interactive use
AddPackage starship # The cross-shell prompt for astronauts
AddPackage thefuck # Magnificent app which corrects your previous console command

# Utils
AddPackage bpytop # Resource monitor that shows usage and stats for processor, memory, disks, network and processes
AddPackage ranger # Simple, vim-like file manager
    AddPackage libcaca # Color AsCii Art library: for ranger Ascii art preview
AddPackage termdown # Countdown timer and stopwatch in your terminal

# Meme
AddPackage cowsay # Configurable talking cow (and a few other creatures)
AddPackage fortune-mod # The Fortune Cookie Program from BSD games
AddPackage lolcat # Okay, no unicorns. But rainbows!!
AddPackage --foreign unimatrix-git # Python script to simulate the display from "The Matrix" in terminal. Uses half-width katakana unicode characters by default, but can use custom character sets.

# Security
AddPackage pass # Stores, retrieves, generates, and synchronizes passwords securely
    AddPackage tree # A directory listing program displaying a depth indented list of files: depenedency for pass
AddPackage libfido2 # Library functionality for FIDO 2.0, including communication with a device over USB
AddPackage yubikey-manager # Python library and command line tool for configuring a YubiKey

# Misc
AddPackage reflector # A Python 3 module and script to retrieve and filter the latest Pacman mirror list.
