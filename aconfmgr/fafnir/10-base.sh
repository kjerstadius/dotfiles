# Baseline packages
AddPackage base # Minimal package set to define a basic Arch Linux installation
AddPackage linux # The Linux kernel and modules
AddPackage linux-firmware # Firmware files for Linux
AddPackage man-db # A utility for reading man pages
AddPackage man-pages # Linux man pages

# Filesystems etc
AddPackage cryptsetup # Userspace setup tool for transparent encryption of block devices using dm-crypt
AddPackage device-mapper # Device mapper userspace library and tools
AddPackage e2fsprogs # Ext2/3/4 filesystem utilities
AddPackage f2fs-tools # Tools for Flash-Friendly File System (F2FS)
AddPackage jfsutils # JFS filesystem utilities
AddPackage mdadm # A tool for managing/monitoring Linux md device arrays, also known as Software RAID
AddPackage nfs-utils # Support programs for Network File Systems
AddPackage lvm2 # Logical Volume Manager 2 utilities
AddPackage reiserfsprogs # Reiserfs utilities
AddPackage xfsprogs # XFS filesystem utilities

# Network
AddPackage dhcpcd # RFC2131 compliant DHCP client daemon
AddPackage ntp # Network Time Protocol reference implementation
AddPackage openssh # Premier connectivity tool for remote login with the SSH protocol

# Bluetooth
AddPackage bluez # Daemons for the bluetooth protocol stack
AddPackage bluez-utils # Development and debugging utilities for the bluetooth protocol stack
CopyFile /etc/bluetooth/main.conf

# Editor
AddPackage neovim # Fork of Vim aiming to improve user experience, plugins, and GUIs
AddPackage python-pynvim # Python client for Neovim - needed for plugins
AddPackage python-jedi # Awesome autocompletion for python, for neovim plugins

# Spelling
AddPackage aspell # A spell checker designed to eventually replace Ispell
AddPackage hunspell # Spell checker and morphological analyzer library and program
AddPackage hunspell-en_gb # GB English hunspell dictionaries
AddPackage hunspell-en_us # US English hunspell dictionaries
AddPackage --foreign hunspell-sv # Swedish dictionaries for Hunspell

# Fonts
AddPackage adobe-source-han-sans-jp-fonts # Adobe Source Han Sans Subset OTF - Japanese OpenType/CFF fonts
AddPackage noto-fonts-emoji # Google Noto emoji fonts
AddPackage ttf-cascadia-code # A monospaced font by Microsoft that includes programming ligatures
AddPackage ttf-fantasque-sans-mono # TTF font family with a great monospaced variant for programmers
AddPackage ttf-fira-code # Monospaced font with programming ligatures
AddPackage ttf-inconsolata # Monospace font for pretty code listings and for the terminal
AddPackage ttc-iosevka # Typeface family designed for coding, terminal use and technical documents.
AddPackage --foreign nerd-fonts-fantasque-sans-mono # Patched font FantasqueSansMono from the nerd-fonts library
AddPackage --foreign nerd-fonts-inconsolata # Patched font Inconsolata from the nerd-fonts library
AddPackage --foreign ttf-inconsolata-g # Monospace font for pretty code listings and for the terminal modified by Leonardo Maffi (http

# Misc
AddPackage --foreign aconfmgr-git # A configuration manager for Arch Linux
AddPackage android-udev # Udev rules to connect Android devices to your linux box
AddPackage exa # ls replacement
AddPackage htop # Interactive process viewer
AddPackage less # A terminal based program for viewing text files
AddPackage lm_sensors # Collection of user space tools for general SMBus access and hardware monitoring
CopyFile /etc/conf.d/lm_sensors
AddPackage logrotate # Rotates system logs automatically
AddPackage lshw # A small tool to provide detailed information on the hardware configuration of the machine.
AddPackage nano # Pico editor clone with enhancements
AddPackage pwgen # Password generator for creating easily memorable passwords
AddPackage ripgrep # A search tool that combines the usability of ag with the raw speed of grep
AddPackage rsync # A file transfer program to keep remote files in sync
AddPackage sudo # Give certain users the ability to run some commands as root
AddPackage sysfsutils # System Utilities Based on Sysfs
AddPackage time # Utility for monitoring a program's use of system resources
AddPackage unrar # The RAR uncompression program
AddPackage usbutils # USB Device Utilities
AddPackage which # A utility to show the full path of commands
AddPackage vi # The original ex/vi text editor
AddPackage zip # Compressor/archiver for creating and modifying zipfiles

# Files
CopyFile /boot/loader/entries/arch-fallback.conf 755
CopyFile /boot/loader/entries/arch.conf 755
CopyFile /boot/loader/loader.conf 755
CopyFile /etc/fstab
CopyFile /etc/group
CopyFile /etc/hostname
CopyFile /etc/hosts
CopyFile /etc/locale.conf
CopyFile /etc/locale.gen
CreateLink /etc/localtime /usr/share/zoneinfo/Europe/Stockholm
CopyFile /etc/makepkg.conf
CopyFile /etc/modprobe.d/alsa.conf
CopyFile /etc/modprobe.d/nfs.conf
CopyFile /etc/modules-load.d/sg.conf
CopyFile /etc/pacman.conf
CopyFile /etc/pacman.d/hooks/100-systemd-boot.hook
CopyFile /etc/pacman.d/mirrorlist
CopyFile /etc/resolv.conf
CopyFile /etc/sddm.conf.d/theme.conf
CopyFile /etc/ssh/sshd_config
CopyFile /etc/subgid
CopyFile /etc/subuid
CopyFile /etc/sudoers
