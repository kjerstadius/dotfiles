# Audio framework
AddPackage alsa-utils # Advanced Linux Sound Architecture - Utilities
AddPackage pulseaudio-alsa # ALSA Configuration for PulseAudio
AddPackage pulsemixer # CLI and curses mixer for pulseaudio
AddPackage --foreign bbfpromix # ALSA Mixer Frontend for RME Babyface Pro (FS)

# Music
AddPackage mpc # Minimalist command line interface to MPD
AddPackage mpd # Flexible, powerful, server-side application for playing music
AddPackage ncmpcpp # Almost exact clone of ncmpc with some new features
AddPackage spotifyd # Lightweigt spotify streaming daemon with spotify connect support
AddPackage --foreign cava # Console-based Audio Visualizer for Alsa

# Video
AddPackage mpv # a free, open source, and cross-platform media player
AddPackage obs-studio # Free, open source software for live streaming and recording
AddPackage youtube-dl # A command-line program to download videos from YouTube.com and a few more sites

# Ripping/encoding etc.
AddPackage beets # Flexible music library manager and tagger
AddPackage ffmpeg # Complete solution to record, convert and stream audio and video
AddPackage mkvtoolnix-cli # Set of tools to create, edit and inspect Matroska files
AddPackage whipper # Python CD-DA ripper preferring accuracy over speed
AddPackage x264 # Open Source H264/AVC video encoder
AddPackage x265 # Open Source H265/HEVC video encoder
AddPackage --foreign makemkv # DVD and Blu-ray to MKV converter and network streamer
AddPackage --foreign rubyripper # Secure audiodisc ripper

# Misc
AddPackage libva-mesa-driver # VA-API implementation for gallium
AddPackage mediainfo # Supplies technical and tag information about a video or audio file (CLI interface)
