# Xorg
AddPackage xorg-server # Xorg X server
AddPackage xorg-xinput # Small commandline tool to configure devices
AddPackage xorg-xrandr # Primitive command line interface to RandR extension
AddPackage xorg-xwayland # run X clients under wayland
CopyFile /etc/X11/xorg.conf.d/00-keyboard.conf

# Display managers
AddPackage sddm # QML based X11 and Wayland display manager
AddPackage --foreign sddm-sugar-dark # The sweetest dark theme around for SDDM, the Simple Desktop Display Manager.

# BSPWM
AddPackage bspwm # Tiling window manager based on binary space partitioning

# i3 environment
AddPackage dunst # Customizable and lightweight notification-daemon
AddPackage feh # Fast and light imlib2-based image viewer
AddPackage i3-gaps # A fork of i3wm tiling window manager with more features, including gaps
AddPackage i3blocks # Define blocks for your i3bar status line
AddPackage --foreign i3lock-color # An improved screenlocker based upon XCB and PAM with color configuration support
AddPackage picom # X compositor that may fix tearing issues
AddPackage rofi # A window switcher, application launcher and dmenu replacement
AddPackage xautolock # An automatic X screen-locker/screen-saver
AddPackage xclip # Command line interface to the X11 clipboard

# Sway environment
AddPackage mako # Lightweight notification daemon for Wayland
    AddPackage jq # Command-line JSON processor: for makoctl menu support
AddPackage sway # Tiling Wayland compositor and replacement for the i3 window manager
    AddPackage inetutils # A collection of common network programs: for resolving config hostname
AddPackage swayidle # Idle management daemon for Wayland
AddPackage wl-clipboard # Command-line copy/paste utilities for Wayland
AddPackage --foreign rofi-lbonn-wayland-git # A window switcher, application launcher and dmenu replacement (Wayland fork)
    AddPackage check # A unit testing framework for C: required for building rofi-lbonn-wayland-git
AddPackage --foreign swaylock-effects-git # A fancier screen locker for Wayland.
    AddPackage scdoc # Tool for generating roff manual pages: makedep for swaylock-effects-git

# River
AddPackage --foreign river # A dynamic tiling wayland compositor.
    AddPackage zig # a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software
AddPackage --foreign waybar-git # Highly customizable Wayland bar for Sway and Wlroots based compositors (GIT)

# KDE
AddPackage dolphin # KDE File Manager
AddPackage kcalc # Scientific Calculator
AddPackage kio-extras # Additional components to increase the functionality of KIO
AddPackage konsole # KDE's terminal emulator
AddPackage kscreen # KDE's screen management software
AddPackage kwin # An easy to use, but flexible, composited Window Manager
AddPackage oxygen # KDE Oxygen style
AddPackage plasma-desktop # KDE Plasma Desktop
AddPackage spectacle # KDE screenshot capture utility
