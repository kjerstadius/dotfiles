# Browsers
AddPackage chromium # A web browser built for speed, simplicity, and security
AddPackage firefox # Standalone web browser from mozilla.org
AddPackage qutebrowser # A keyboard-driven, vim-like browser based on PyQt5
AddPackage --foreign brave-bin # Web browser that blocks ads and trackers by default (binary release)

# Terminal
AddPackage kitty # A modern, hackable, featureful, OpenGL-based terminal emulator

# Chat
AddPackage discord # All-in-one voice and text chat for gamers that's free and secure.

# CAD
AddPackage freecad # General purpose 3D CAD modeler
AddPackage librecad # A 2D CAD drawing tool based on the community edition of QCad
AddPackage openscad # The programmers solid 3D CAD modeller
AddPackage --foreign front-panel-designer-eu # Free CAD software for front panel design

# EDA
AddPackage kicad # Electronic schematic and printed circuit board (PCB) design tools
AddPackage kicad-library # Kicad component and footprint libraries
AddPackage kicad-library-3d # Kicad 3D render model libraries

# Graphics
AddPackage krita # Edit and paint images
    AddPackage libheif # HEIF file format decoder and encoder: for HEIF support
AddPackage inkscape # Professional vector graphics editor
    AddPackage gvfs # Virtual filesystem implementation for GIO: for inkscape clipart support

# pdf viewer
AddPackage zathura-pdf-mupdf # PDF support for Zathura (MuPDF backend) (Supports PDF, ePub, and OpenXPS)

# Other
AddPackage yubikey-manager-qt # Cross-platform application for configuring any YubiKey over all USB transports
