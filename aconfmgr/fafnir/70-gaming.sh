# Platforms
AddPackage lutris # Open Gaming Platform
    AddPackage gvfs # Virtual filesystem implementation for GIO: for lutris gvfs backend (?)
AddPackage steam # Valve's digital software delivery system

# Retroarch
AddPackage libretro-flycast # Sega Dreamcast core
AddPackage retroarch # Reference frontend for the libretro API
AddPackage retroarch-assets-ozone # XMB menu assets for RetroArch
AddPackage retroarch-assets-xmb # XMB menu assets for RetroArch

# Emulation
AddPackage --foreign dolphin-emu-git # A Gamecube / Wii emulator - git version
AddPackage --foreign rpcs3-git # A Sony PlayStation 3 emulator
    AddPackage vulkan-validation-layers # Required for building rpcs3

# Game engines (non-emulation)
AddPackage --foreign rbdoom3-bfg-git # Doom 3 BFG Edition with soft shadows, cleaned up source, Linux and 64 bit Support

# Tools
AddPackage gamemode # A daemon/lib combo that allows games to request a set of optimisations be temporarily applied to the host OS
AddPackage lib32-gamemode # A daemon/lib combo that allows games to request a set of optimisations be temporarily applied to the host OS
AddPackage --foreign path-of-building-community-git # An offline build planner for Path of Exile using PoBFrontent, LocalIdentity's fork

# Libraries etc.
AddPackage lib32-gst-plugins-base-libs # GStreamer Multimedia Framework Base Plugin libraries (32-bit)
AddPackage lib32-mpg123 # A console based real time MPEG Audio Player for Layer 1, 2 and 3 (32-bit)
AddPackage wine-mono # Wine's built-in replacement for Microsoft's .NET Framework
AddPackage wine-staging # A compatibility layer for running Windows programs - Staging branch
