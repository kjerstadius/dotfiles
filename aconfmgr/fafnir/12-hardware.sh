# CPU microcode
AddPackage amd-ucode # Microcode update files for AMD CPUs

# Video drivers
AddPackage vulkan-radeon # Radeon's Vulkan mesa driver
AddPackage xf86-video-amdgpu # X.org amdgpu video driver
CopyFile /etc/X11/xorg.conf.d/20-amdgpu.conf

# 32-bit packages
AddPackage lib32-vulkan-icd-loader # Vulkan Installable Client Driver (ICD) Loader (32-bit)
AddPackage lib32-vulkan-radeon # Radeon's Vulkan mesa driver (32-bit)
